﻿namespace MailMotion.Repository.Db
{
    public interface IUnitOfWork
    {
        void Commit();
        bool LazyLoadingEnabled { get; set; }
        bool ProxyCreationEnabled { get; set; }
        string ConnectionString { get; set; }
    }
}