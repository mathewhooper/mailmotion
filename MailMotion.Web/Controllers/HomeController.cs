﻿namespace MailMotion.Web.Controllers
{
    using System.Web.Mvc;

    public class HomeController : Controller
    {
       // Default action to log users in
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }
    }
}
