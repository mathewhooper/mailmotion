﻿namespace MailMotion.Repository.Model.Marketing
{
    using System.ComponentModel.DataAnnotations;

    public class Campaign : EntityDate
    {
        [MaxLength(50, ErrorMessage = "The name of the campaign cannot be longer than 50 characters")]
        public string Name { get; set; }

        // TODO: Need to add who created the record
    }
}