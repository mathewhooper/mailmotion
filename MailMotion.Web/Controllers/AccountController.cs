﻿namespace MailMotion.Web.Controllers
{
    using System.Web.Mvc;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Repository.Model.Authentication;

    [RequireHttps, Authorize]
    public class AccountController : Controller
    {
        private AccountController(UserManager<MailMotionUser> userManager)
        {
            UserManager = userManager;
        }

        private UserManager<MailMotionUser> UserManager { get; set; }

        public ActionResult Index()
        {
            return View();
        }
	}
}