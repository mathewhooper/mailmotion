﻿namespace MailMotion.Repository
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using Model.Authentication;

    public class MailMotionContext : IdentityDbContext<MailMotionUser>
    {
        public MailMotionContext () : base("MailMotion") {}
    }
}