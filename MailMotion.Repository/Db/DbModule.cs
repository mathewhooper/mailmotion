﻿namespace MailMotion.Repository.Db
{
    using Ninject.Modules;

    public class DbModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IDbFactory>().To<DbFactory>().InSingletonScope();
            Bind<IUnitOfWork>().To<UnitOfWork>().InSingletonScope();
        }
    }
}