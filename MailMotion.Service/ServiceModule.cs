﻿namespace MailMotion.Service
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Ninject.Modules;
    using Ninject.Parameters;
    using Repository;
    using Repository.Model.Authentication;

    public class ServiceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserStore<MailMotionUser>>()
                .To<UserStore<MailMotionUser>>()
                .WithConstructorArgument(new ConstructorArgument("context", new MailMotionContext()));
            Bind<IRoleStore<MailMotionRole>>()
                .To<RoleStore<MailMotionRole>>()
                .WithConstructorArgument(new ConstructorArgument("context", new MailMotionContext()));
        }
    }
}