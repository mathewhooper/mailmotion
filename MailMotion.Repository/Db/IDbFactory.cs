﻿namespace MailMotion.Repository.Db
{
    using System;

    public interface IDbFactory : IDisposable
    {
        MailMotionContext Get();
    }
}