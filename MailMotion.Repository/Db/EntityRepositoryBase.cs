﻿namespace MailMotion.Repository.Db
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;

    public abstract class EntityRepositoryBase<T> where T : class
    {
        protected MailMotionContext _dataContext;
        protected readonly IDbSet<T> _dbSet;

        protected EntityRepositoryBase(IDbFactory dbFactory)
        {
            this.DbFactory = dbFactory;
            _dbSet = this.DataContext.Set<T>();
        }

        protected IDbFactory DbFactory { get; private set; }

        protected MailMotionContext DataContext
        {
            get { return _dataContext ?? (_dataContext = DbFactory.Get()); }
        }

        public virtual void Add(T entity)
        {
            _dbSet.Add(entity);
        }

        public virtual void Update(T entity)
        {
            _dataContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            _dbSet.Remove(entity);
        }

        public void Delete(Func<T, Boolean> where)
        {
            IEnumerable<T> objects = _dbSet.Where<T>(where).AsEnumerable();
            foreach (T obj in objects)
                _dbSet.Remove(obj);
        }

        public virtual T GetById(long id)
        {
            return _dbSet.Find(id);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return _dbSet.ToList();
        }

        public virtual IEnumerable<T> GetSelection(Func<T, bool> where)
        {
            return _dbSet.Where(where).ToList();
        }

        public T Get(Func<T, Boolean> where)
        {
            return _dbSet.Where(where).FirstOrDefault<T>();
        } 
    }
}