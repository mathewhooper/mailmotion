﻿namespace MailMotion.Repository.Db
{
    using System;
    using System.Collections.Generic;

    public interface IEntityRepository<T> where T : class
    {
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(Func<T, Boolean> predicate);
        T GetById(long id);
        T Get(Func<T, Boolean> where);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetSelection(Func<T, bool> where);
    }
}