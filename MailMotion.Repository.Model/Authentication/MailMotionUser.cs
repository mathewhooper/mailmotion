﻿namespace MailMotion.Repository.Model.Authentication
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    public class MailMotionUser : IdentityUser, IUser<string>
    {
         
    }
}