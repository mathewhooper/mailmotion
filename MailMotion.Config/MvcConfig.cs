﻿namespace MailMotion.Config
{
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;

    public class MvcConfig
    {
        public MvcConfig BundleConfig(BundleCollection bundleCollection)
        {
            BundleTable.EnableOptimizations = MailMotionConfig.EnableOptimizations;

            // Bootstrap and Normalize
            bundleCollection.Add(new StyleBundle("~/assets/bootstrap").Include(
                "~/assets/css/Shared/Normalize.css",
                "~/assets/css/Shared/bootstrap.css"
            ));

            // MailMotion themes
            bundleCollection.Add(new StyleBundle("~/assets/mailmotion-site").Include(
                "~/assets/css/Shared/generic.css",
                "~/assets/css/Shared/header.css",
                "~/assets/css/Shared/menu.css",
                "~/assets/css/Shared/footer.css"
            ));
            bundleCollection.Add(new StyleBundle("~/assets/mailmotion-login").Include(
                "~/assets/css/Shared/generic.css",
                "~/assets/css/Shared/login.css"
            ));

            bundleCollection.Add(new ScriptBundle("~/assets/jquery").Include(
                "~/assets/javascript/Vendor/query.js"
            ));
            bundleCollection.Add(new ScriptBundle("~/assets/modernizr").Include(
                "~/assets/javascript/Vendor/modernizr.js"
            ));
            bundleCollection.Add(new ScriptBundle("~/assets/bootstrapjs").Include(
                "~/assets/javascript/Vendor/bootstrap.js"
            ));


            return this;
        }

        public MvcConfig FilterConfig(GlobalFilterCollection globalFilterCollection)
        {
            return this;
        }

        public MvcConfig RouteConfig(RouteCollection routeCollection)
        {
            routeCollection.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routeCollection.MapRoute(
                "Default Route",
                "{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            return this;
        }

        public MvcConfig WebApiRouteConfig(HttpConfiguration httpConfiguration)
        {
            return this;
        }
    }
}