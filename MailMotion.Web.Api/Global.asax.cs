﻿namespace MailMotion.Web.Api
{
    using System.Reflection;
    using System.Web.Http;
    using System.Web.Mvc;
    using Config;
    using Ninject;
    using Ninject.Web.Common;

    public class WebApiApplication : NinjectHttpApplication
    {
        protected override IKernel CreateKernel()
        {
            var kernel = NinjectWebCommon.CreateKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            return kernel;
        }

        protected override void OnApplicationStarted()
        {
            base.OnApplicationStarted();

            AreaRegistration.RegisterAllAreas();

            new MvcConfig()
                .FilterConfig(GlobalFilters.Filters)
                .WebApiRouteConfig(GlobalConfiguration.Configuration);
        }
    }
}