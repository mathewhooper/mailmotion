﻿namespace MailMotion.Repository.Db
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbFactory _dbFactory;
        private MailMotionContext _dataContext;

        public UnitOfWork(IDbFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }

        protected MailMotionContext DataContext
        {
            get { return _dataContext ?? (_dataContext = _dbFactory.Get()); }
        }

        public void Commit()
        {
            DataContext.SaveChanges();
        }

        public bool LazyLoadingEnabled
        {
            get { return _dataContext.Configuration.LazyLoadingEnabled; }
            set { _dataContext.Configuration.LazyLoadingEnabled = value; }
        }
        public bool ProxyCreationEnabled
        {
            get { return _dataContext.Configuration.ProxyCreationEnabled; }
            set { _dataContext.Configuration.ProxyCreationEnabled = value; }
        }
        public string ConnectionString
        {
            get { return _dataContext.Database.Connection.ConnectionString; }
            set { _dataContext.Database.Connection.ConnectionString = value; }
        }
    }
}