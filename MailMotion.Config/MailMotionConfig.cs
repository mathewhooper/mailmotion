﻿namespace MailMotion.Config
{
    using InfiniteGeek.Util;

    public class MailMotionConfig : AutoConfig<MailMotionConfig>
    {
        static MailMotionConfig()
        {
            Init();
        }

        public static bool EnableOptimizations { get; private set; }
    }
}