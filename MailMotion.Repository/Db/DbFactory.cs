﻿namespace MailMotion.Repository.Db
{
    using System.Data.Entity;

    public class DbFactory : Disposable, IDbFactory
    {
        private MailMotionContext _dataContext;

        public DbFactory()
        {
            Database.SetInitializer<MailMotionContext>(null);
        }

        public MailMotionContext Get()
        {
            return _dataContext ?? (_dataContext = new MailMotionContext());
        }

        protected override void DisposeCore()
        {
            if (_dataContext != null)
                _dataContext.Dispose();
        }
    }
}