﻿namespace MailMotion.Repository.Model
{
    using System;

    public abstract class EntityDate : Entity
    {
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}