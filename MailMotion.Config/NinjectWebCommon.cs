﻿namespace MailMotion.Config
{
    using System.Reflection;
    using Ninject;
    using Repository.Db;

    public static class NinjectWebCommon
    {
        public static IKernel CreateKernel()
        {
            var kernel = new StandardKernel(
                //new ConfigModule(),
                new DbModule()
            );
            kernel.Load(Assembly.GetExecutingAssembly());
            return kernel;
        }
    }
}