﻿namespace MailMotion.Web
{
    using System.Reflection;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Mvc;
    using Config;
    using Ninject;
    using Ninject.Web.Common;
    using StackExchange.Profiling;

    public class MvcApplication : NinjectHttpApplication
    {
        protected void Application_BeginRequest()
        {
            if (Request.IsLocal)
            {
                MiniProfiler.Start();
                MiniProfilerEF.InitializeEF42();
            }
        }

        protected void Application_EndRequest()
        {
            MiniProfiler.Stop();
        }

        protected override void OnApplicationStarted()
        {
            base.OnApplicationStarted();

            AreaRegistration.RegisterAllAreas();

            new MvcConfig()
                .BundleConfig(BundleTable.Bundles)
                .FilterConfig(GlobalFilters.Filters)
                .RouteConfig(RouteTable.Routes);
        }

        protected override IKernel CreateKernel()
        {
            var kernel = NinjectWebCommon.CreateKernel();
            kernel.Load(Assembly.GetExecutingAssembly());

            return kernel;
        }
    }
}