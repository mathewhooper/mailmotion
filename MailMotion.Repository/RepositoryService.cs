﻿namespace MailMotion.Repository
{
    using Ninject.Modules;

    public class RepositoryService : NinjectModule
    {
        public override void Load()
        {
            Bind<MailMotionContext>().ToSelf();
        }
    }
}